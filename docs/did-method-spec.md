# SiriusId DID Method Specification

v0.2, Proximax Ltd

## Overview

The ProximaX SiriusID DID method specification conforms to the requirements specified in the DID specification currently published by the W3C Credentials Community Group. For more information about DIDs and DID method specifications, please see the [DID Primer](https://github.com/WebOfTrustInfo/rwot5-boston/blob/master/topics-and-advance-readings/did-primer.md and [DID Spec](https://w3c-ccg.github.io/did-spec/). Its core technologies are the [ProximaX Sirius Chain Platform](https://proximax.io) and [ProximaX Storage](https://proximax.io)

ProximaX SiriusID is intended to implement `did:sirius` and its DID Document.

## Target System

The target system is the ProximaX Sirius Chain network. This can either be:

- Sirius Chain on Main Net
- Sirius Chain on Test Net
- Sirius Chain on Private Net

## Method Name

The name string that shall identify this DID method is `sirius`

A DID that uses this method MUST begin with the following prefix: `did:sirius`. Per the DID specification, this string MUST be in lowercase. The remainder of the DID, after the prefix, is specified below.

## Method Specific Identifier

The SiriusID DID schema is defined by the following:

```abnf
siriusid-did = "did:sirius:" idstring

idstring = base58(version,network-identifier,address)
```

- version: the version of the did, default is 1.0
- network-identifier: the ProximaX Chain network type, default is MAIN_NET
- address: the ProximaX Sirius Registry unique account address

### Example

A valid SiriusID DID might be:

```abnf
did:sirius:2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj
```

## Operation Definitions

### Create

Creating the new `did:sirius` value consists of creating an account in the ProximaX Sirius Registry and encoding the account address
using the format provided in the [method specific identifier](#Method-Specific-Identifier) section.

Account key pair:

The key pair consists of a private key and a public key generated using the Twisted Edwards curve Ed25519 digital signature algorithm.

- Private key: A random 256-bit integer used to sign entities.
- Public key: The public identifier of the key pair. Proves that the entity was signed with the paired private key. The public key is cryptographically derived from the private key.

The account address is a base-32 encoded triplet consisting of:

- The network byte.
- The 160-bit hash of the account’s public key.
- The 4-bytes checksum, to allow a quick recognition of mistyped addresses.

The creation of a DID Document is also performed by taking the public key value and expanding it into DID Document.

The minimum `did:sirius` DID document for an identifier e.g., `2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj` looks like this

```jsonld
{
  "@context": ['https://w3id.org/did/v1', 'https://w3id.org/security/v2'],
  "id": "did:sirius:2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj",
  "authentication": [
    "did:sirius:2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj#keys-1"
  ]
  "publicKey": [{
    "id": "did:sirius:2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj#signing-key-1",
    "type": "Ed25519VerificationKey2018",
    "controller": "did:sirius:2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj",
    "publicKeyHex": "58773C5D5DBE4339FDFD1267F1D5E60127C5BC4760E6E884D9E7ABCD57994748"
  }]
}
```

### DID Document Registration

The DID Document registration process below:

1. Publish the DID Document content to the public ProximaX Sirius Storage (DFMS). The response returns with a unique content URL.
2. Generate a transaction payload with the DID Document content URL and sign it with the created account in the [Create](#Create) section above.
3. Make a transaction request with the generated payload to the ProximaX Sirius Registry.
4. The DID Document generated payload will be registered in the ProximaX Sirius Registry.

Note: To make the transaction request to the ProximaX Sirius Registry, a small amount of XPX needs to be present on the ProximaX Sirius Registry account.

### Read

Reading a `did:sirius` value is a matter of resolving the DID Document.

On-chain

The DID document can be resolved by the given `did:sirius` value via the ProximaX Sirius Registry. The ProximaX Sirius Registry parses the `did:sirius` value to obtain the ProximaX Sirius Chain network and the account address.

From the extracted information, query the ProximaX Sirius Chain to retrieve the `did:sirius` DID Document content URL from the [registration](#DID-Document-Registration) process above.

The `did:sirius` DID Document content URL can be downloaded via the ProximaX Sirius Storage.

Off-Chain

You can resolve DID Document without connecting to the ProximaX Sirius Registry. The process is simply expanding the `did:sirius` value to the DID Document.

Note: Retrieving DID Document off-chain will return a bare minimum DID Document content. For example,

Resolving the `did:sirius:2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj` value returns

```jsonld
{
  "@context": ['https://w3id.org/did/v1', 'https://w3id.org/security/v2'],
  "id": "did:sirius:2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj",
  "authentication": [
    "did:sirius:2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj#keys-1"
  ]
  "publicKey": [{
    "id": "did:sirius:2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj#signing-key-1",
    "type": "Ed25519VerificationKey2018",
    "controller": "did:sirius:2U6DBRZEYb8Cnv1m9icQTdJmi6rhKXFzc3Eeo5Pqbj",
    "publicKeyHex": "58773C5D5DBE4339FDFD1267F1D5E60127C5BC4760E6E884D9E7ABCD57994748"
  }]
}
```

### Update

Whenever the `did:sirius` DID Document is modified, the document hash will change. The owner of the `did:sirius` DID Document need to publish the new DID Document via the ProximaX Sirius Registry again, following the [registration](#DID-Document-Registration) process.

### Delete

The registered `did:sirius` DID Document can be deactivated by simply making a signed transaction request to the ProximaX Sirius Registry.

The ProximaX Sirius Registry will remove the associated DID Document content URL from the [created](#Create) account.

Note: To make the transaction request to the ProximaX Sirius Registry, a small amount of XPX needs to be present on the ProximaX Sirius Registry account.

## Security and Privacy Considerations

This section is non-normative.

There are several securities and privacy considerations that implementers will want to take into consideration when implementing this specification.

### Key Recovery

The `did:sirius` method is a purely generative method from the ProximaX Sirius Registry account key pair, which means that updates are not supported.

If the account private key is ever compromised, it is not possible to replace the compromised key. Thus, the account owner needs to ensure the account private key is the best-kept secret.

To minimize the risk of account private key compromise, using security hardware for key storage is highly recommended.

### Key Revocation

There is no centralized control of the ProximaX Sirius Registry account key pair. Hence, no party can revoke the account of the `did:sirius` value within the ProximaX Sirius platform.

### Privacy

No personal information stored in the `did:sirius` DID document. The `did:sirius` DID document should be limited to just public keys and service endpoints.

The public key will be used to encrypt data, and only those with the private key can decrypt and access the data.

The private key only exists on the user's device and will not be known to any third party.

## Reference

- [DID Method Registry](https://w3c.github.io/did-spec-registries/#did-methods)
