# ProximaX Sirius ID Verifiable Credentials Specification
v0.1, Thomas Tran

## Overview

ProximaX Sirius ID Verifiable Credentials (Credentials) implementation based on the stardard Credential and Presentation models [W3C Verifiable Credentials Data Model 1.0](https://w3c.github.io/vc-data-model/) document.

Users will be able to view, create and remove ProximaX Sirius ID Credentials (pre-defined or custom). Besides, those credentials can be stored on the device local database
off-chain or on the ProximaX Sirius Chain via the ProximaX Sirius ID Registry.

To make credentials easily manageable, the credentials will be represented in JSON_LP + JSON Web Token(JWT) format based on the [Verifiable Credentials Implementation Guidelines 1.0 ](https://w3c.github.io/vc-imp-guide/) document.

A JWT token consists of three parts: header, payload, and signature.

### Credential Header

### Credential Payload

### Credential Signature

### Credential Presentation Header

### Credential Presentation Payload

### Credential Presentation Signature

## Pre-defined Credentials

## Custom Credentials

## On-Chain

## Off-Chain

